<?php

namespace App\Http\Mappers;

use App\Http\Interfaces\Mapper;
use App\Http\Sources\CsvSource;
use App\Http\Sources\DatabaseSource;
use App\Http\Sources\JsonSource;

class DataSourceMapper implements Mapper
{
    public static function getMap() : array {
        return [
            'json' => JsonSource::class,
            'csv' => CsvSource::class,
            'db' => DatabaseSource::class,
        ];
    }
}
