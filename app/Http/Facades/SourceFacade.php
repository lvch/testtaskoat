<?php

namespace App\Http\Facades;

use App\Http\Interfaces\SourceInterface;
use App\Http\Mappers\DataSourceMapper;
use Illuminate\Database\Eloquent\Model;

class SourceFacade
{
    private SourceInterface $sourceClass;

    public function __construct() {
        $class = array_key_exists(
            env('DATA_SOURCE', 'db'),
            DataSourceMapper::getMap()
        )   ? DataSourceMapper::getMap()[env('DATA_SOURCE', 'db')]
            : DataSourceMapper::getMap()['db'];

        $this->sourceClass = new $class();
    }

    /**
     * @var Model
     * @return array
     */
    public function all($class) {
        return $this->sourceClass->all(new $class());
    }

    /**
     * @param Model $model
     * @return array
     */
    public function save(Model $model) {
        return $this->sourceClass->save($model);
    }

}
