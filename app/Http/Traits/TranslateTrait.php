<?php


namespace App\Http\Traits;


use Stichoza\GoogleTranslate\GoogleTranslate;

trait TranslateTrait
{
    /**
     * @var array
     * This variable must be associated array, where key is a field from array and value is a type for field.
     * Value could be the follow:
     * * 'string'
     * * 'Classname:array', where Classname is a name of a needed class
     * * 'Classname', where Classname is a name of a needed class
     */
    public static array $fieldsToTranslate;

    public static function translateArray(array $arr, GoogleTranslate $translator)
    {
        // TODO: Translate array
        return $arr;
    }

    public static function translate(string $str, GoogleTranslate $translator)
    {
        // TODO: Translate array
    }
}
