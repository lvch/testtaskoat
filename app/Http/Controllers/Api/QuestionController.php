<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Resources\QuestionResource;
use App\Models\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Stichoza\GoogleTranslate\GoogleTranslate;

class QuestionController extends Controller
{
    const COUNT_CHOICES = 3;
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     * @var GoogleTranslate
     */
    public function index(GoogleTranslate $lang)
    {
        $question = Question::getAll($lang);
        return response(['projects' => QuestionResource::collection($question), 'message' => 'List of translated questions and associated choices'], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = json_decode($request->getContent());

        if (!property_exists($data, 'choices')
            || (property_exists($data, 'choices') && count($data->choices) != COUNT_CHOICES)
            || !property_exists($data, 'text')
        ) {
            return response(['error' => 'the number of associated choices must be exactly equal to 3 and request has to have a text field', 'Validation Error']);
        }

        $question = Question::create((array) $data);

        return response(['question' => new QuestionResource($question), 'message' => 'Created successfully'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Question $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Question $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Question $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        //
    }
}
