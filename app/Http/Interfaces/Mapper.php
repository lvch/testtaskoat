<?php

namespace App\Http\Interfaces;

interface Mapper
{
    /**
     * @return array
     */
    public static function getMap() : array;
}
