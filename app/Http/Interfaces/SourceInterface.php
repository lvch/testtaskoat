<?php

namespace App\Http\Interfaces;

use Illuminate\Database\Eloquent\Model;

interface SourceInterface
{
    /**
     * @param Model $model
     * @return array
     */
    public function all(Model $model);

    /**
     * @param Model $model
     * @return array
     */
    public function save(Model $model);
}
