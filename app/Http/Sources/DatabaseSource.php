<?php

namespace App\Http\Sources;

use App\Http\Interfaces\SourceInterface;
use Illuminate\Database\Eloquent\Model;

class DatabaseSource implements SourceInterface
{

    public function all(Model $model)
    {
        // TODO: Implement all() method.
    }

    public function save(Model $model)
    {
        // TODO: Implement save() method.
    }
}
