<?php

namespace App\Models;

use App\Http\Traits\TranslateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Choice extends AbstractModel
{
    use TranslateTrait, HasFactory;

    public static array $fieldsToTranslate = [
        'text' => 'string',
    ];

    /**
     * @var string
     */
    private string $text;

}
