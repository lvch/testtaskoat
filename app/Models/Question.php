<?php

namespace App\Models;

use App\Http\Facades\SourceFacade;
use App\Http\Traits\TranslateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stichoza\GoogleTranslate\GoogleTranslate;

class Question extends AbstractModel
{
    use TranslateTrait, HasFactory;

    public $fillable = [
        'text',
        'createdAt'
    ];

    public static array $fieldsToTranslate = [
        'text' => 'string',
        'choices' => 'Choice:array'
    ];

    /**
     * @var string
     */
    private string $text;

    /**
     * @var string
     * format: date-time
     * Creation date of the question
     */
    private string $createdAt;

    /**
     * @var Choice[]
     * Choices associated to the question
     * @see Choice
     */
    private array $choices = [];

    public static function create(array $data) {
        $question = new Question();
        $question->text = $data['text'];
        $question->createdAt = new \DateTime();
        foreach ($data['choices'] as $key => $value) {
            $choice = new Choice();
            $choice->text = $value->text;
            array_merge($question->choices, $choice->toArray());
        }

        $question->save();
    }

    public function choices() {
        return $this->hasMany('App\Models\Choice');
    }

}
