<?php


namespace App\Models;


use App\Http\Facades\SourceFacade;
use App\Http\Traits\TranslateTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Stichoza\GoogleTranslate\GoogleTranslate;

class AbstractModel extends Model
{
    use TranslateTrait, HasFactory;

    /**
     * @var SourceFacade
     */
    protected SourceFacade $source;

    public function __construct(array $attributes = []) {
        $this->source = new SourceFacade();
        parent::__construct($attributes);
    }

    public static function getAll(GoogleTranslate $translator) {
        return self::translateArray (
            (new static)->getSource()->all(__CLASS__),
            $translator
        );
    }

    public function save(array $options = []) {
        $this->getSource()->save($this);
    }

    public function getSource() {
        return $this->source;
    }
}
