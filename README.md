<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## About Project

This is test task for OAT company. I hope, that you will have questions. And 
you will want to ask them to me. Because I think, that I can explain my thoughts face ot face.
And that will be better =)

Routes for the API you can find in ./routes/api.php

I tried to do basic architecture for visualisation my thoughts.

## Available formats
Right now available csv, json and db.

If you want to choose one of them change DATA_SOURCE value in .env.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
